const wrapper = document.querySelector('.wrapper');
const btn = document.querySelector('.btn');
const self = document.createElement('div');
const continent = document.createElement('p');
const country = document.createElement('p');
const regionName = document.createElement('p');
const city = document.createElement('p');
const district = document.createElement('p');

btn.addEventListener('click', onclickBtn);

async function onclickBtn() {
    const ip = await fetch('https://api.ipify.org/?format=json')
        .then(response => response.json())
        .then(data => data.ip);

    const info = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`)
        .then(response => response.json());
    continent.textContent = "continent: " + info.continent;
    country.textContent = "country: " + info.country;
    regionName.textContent = "region: " + info.regionName;
    city.textContent = "city: " + info.city;
    district.textContent = "district: " + info.district;
    self.style.border = "1px solid black";
    self.append(
        continent,
        country,
        regionName,
        city,
        district
    );
    wrapper.append(self)
}